public class QueenProblem {
    public static void main(String[] args) {
        if(args.length == 0) {
            System.out.println("Please enter the coordinate you wish to place the Queen. (For example, A0)");
            System.exit(0);
        }

        // Input args for placement of queen
        String input = args[0];
        char theRow = input.charAt(0);
        int row = 9;
        
        if(theRow == 'A' || theRow == 'a'){
            row = 0;
        }
        else if(theRow == 'B' || theRow == 'b'){
            row = 1;
        }
        else if(theRow == 'C' || theRow == 'c'){
            row = 2;
        }
        else if(theRow == 'D' || theRow == 'd'){
            row = 3;
        }
        else if(theRow == 'E' || theRow == 'e') {
            row = 4;
        }
        else if(theRow == 'F' || theRow == 'f'){
            row = 5;
        }
        else if(theRow == 'G' || theRow == 'g'){
            row = 6;
        }
        else if(theRow == 'H' || theRow == 'h'){
            row = 7;
        }
        else {
            row = 8;
        }

        int col = Integer.parseInt(input.substring(1));

        if (row < 8 && col < 8) {
            Queens queen = new Queens(row, col);
            queen.printSolution();
        } else {
            System.out.println("Invalid coordinates!");
        }
    }

  

}