import java.util.Arrays;

public class Queens {
    public static final int N = 8;
    public static int counter;
    public int row, column;

    public Queens(int row, int column) {
        this.row = row;
        this.column = column;
    }

    private static boolean isSafe(char mat[][], int r, int c) {
        // Checks if there's a queen in the column above
        for (int i = 0; i < r; i++) {
            if (mat[i][c] == 'Q') {
                return false;
            }
        }
        // Checks if there's a queen in the row to the left
        for (int j = 0; j < c; j++) {
            if (mat[r][j] == 'Q') {
                return false;
            }
        }
        // Checks if there's a queen diagonally
        for (int i = r, j = c; i >= 0 && j >= 0; i--, j--) {
            if (mat[i][j] == 'Q') {
                return false;
            }
        }
        // Checks if there's a queen diagonally
        for (int i = r, j = c; i >= 0 && j < N; i--, j++) {
            if (mat[i][j] == 'Q') {
                return false;
            }
        }
        return true;
    }

    // Print solution if queens are placed correctly.
    private static void nQueen(char mat[][], int r, int row, int col) {
        if (r == N) {
            // Loop through mat and compare to args. If equal, print the current chess
            // board.
            if (mat[row][col] == 'Q') {
                for (int i = 0; i < N; i++) {
                    for (int j = 0; j < N; j++) {
                        System.out.print(mat[i][j] + " ");
                    }
                    System.out.println();
                }
                System.out.println();
                counter++;
            }
            return;
        }
        // Checks if space is empty or not, then assigns a Q to the empty slot.
        for (int i = 0; i < N; i++) {
            if (isSafe(mat, r, i)) {
                mat[r][i] = 'Q';
                nQueen(mat, r + 1, row, col);
                mat[r][i] = '-';
            }
        }
    }

    public void printSolution() {
        // Mat is an array with 8 arrays with 8 slots.
        char[][] mat = new char[N][N];
        // Fills mats array number i's 8 slots with "-". The iteration makes it go
        // through all 8 of mats arrays.
        for (int i = 0; i < N; i++) {
            Arrays.fill(mat[i], '-');
        }

        nQueen(mat, 0, row, column);
        System.out.println("Above are all of the possible positions with your queen at row: " + row + " and column: " + column);
        System.out.println("There are a total of " + counter + " solutions for the coordinates you've entered.");
    }
}